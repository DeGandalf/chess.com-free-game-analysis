# Chess.com free game analysis

This is a Tampermokey script, which adds a button to the chess.com analysis and game result pages, which redirects you automatically to the free analysis of the game on lichess.org

## What you need

- Tampermonkey
- Free lichess.org account

## Install

- From [GitLab](https://gitlab.com/DeGandalf/chess.com-free-game-analysis/-/raw/main/chess-analysis.user.js) <- just click this link to install

## The button is located at the bottom of the sidebar

![](images/overview.png)